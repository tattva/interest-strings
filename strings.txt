[[General]]
	[cancel]
		en = Cancel
		ja = キャンセル
		zh = 取消
	[connection_error_message]
		en = There was a problem connecting to the server. Please check your internet connection and try again.
		zh = 网络连接出错。请检查您的网络设置然后重试。
	[reload_button]
		en = Reload
		zh = 重新载入
    [load_more_button]
        en = Load More
	[error]
		en = Error
		ja = エラー
		zh = 错误
	[done]
		en = Done
		zh = 完成
    [next]
        en = Next
		zh = 继续
	[connection_error]
		en = Connection Error
		zh = 连接错误
	[unknown_error_message]
		en = An unknown error occurred. Please try again.
		zh = 发生未知错误。请重试。
    [retry]
        en = Retry
		zh = 重试
    [yes]
        en = Yes
		zh = 是
    [no]
        en = No
		zh = 否
    [view_notification]
        en = View
		zh = 浏览
    [ok]
        en = OK
		zh = OK
    [loading]
        en = Loading...
		zh = 载入中...
    [success]
        en = Success!
    [no_objects]
    	en = Nothing to display.
	[empty_input_error]
		en = Please input text.

[[+in list]]
	[no_ins_in_list]
		en = No Images.
		zh = 没有图片。

[[Login]]
    [login_screen_message]
        en = Discover anime you love and deepen your interest.
		zh = 发现您喜爱的动漫，让您爱不释手。
	[generic_login_error]
		en = There was an error logging in. Please try again.
		zh = 登录发生错误，请重试。
    [agree_tos_pp]
        en = By logging in you agree to the %@ and %@
		zh = 需要阅读并同意%@和%@
    [tos]
        en = Terms of Use
		zh = 使用规则
    [must_agree_tos]
        en = Please read and agree to the Terms of Service and Privacy Policy before logging in.
        zh = 在您登录前，您必须阅读和同意使用规则和隐私条款。
    [login_with]
        en = Login With...
        comment = e.g. login with Facebook, twitter, google
    [choose_account]
        en = Please choose the account you want to log in with.
        comment = On iOS, for choosing which twitter account to use if the user has multiple accounts.
    [permission_denied_title]
        en = Permission Denied
    [permission_denied_twitter_msg]
        en = Please grant access to Twitter in iOS Settings app.\nSettings > Twitter > i.ntere.st ON
    [twitter_unavailable_title]
        en = Twitter Account Unavailable
    [twitter_unavailable_msg]
        en = Please add your Twitter account in iOS settings first.
    [register_account_title]
        en = Register Account
    [login_button]
        en = Login
    [register_button]
        en = Register
    [login_existing_button]
        en = Already have an account?
    [register_new_button]
        en = Don't have an account?
    [password]
        en = Password
    [password_confirm]
        en = Re-enter Password
	[empty_value]
		en = Please input Username and Password.
	[error_password_confirm]
		en = Password is not matched. Please check and try.

[[+in Detail Screen]]
	[posted_in_rewarded_msg]
		en = You have successfully +in-ed this image and now have %d points!
		zh = 您成功的+in了这张图片并且获得了%d积分。
	[posted_in_duplicate]
		en = Oops! You have already +in-ed this image!
		zh = 您已经+in过了这张图片。
    [suspend_in_message]
        en = No points added.\nYou've reached the daily limit of %d points.
		zh = 没有获得积分。\n您已经达到了日均获取积分的上限。
	[post_another]
		en = Another
		zh = 其他
	[delete_in_confirmation]
		en = Are you sure you want to delete this +in?
		zh = 是否确认删除此+in？
	[delete_in_action]
		en = Delete
		zh = 删除
	[in_deleted]
		en = +in was deleted successfully.
		zh = 已经成功删除。
	[image_saved]
		en = Saved image to your photo library.
		zh = 保存图片到手机相册
	[tags_updated]
		en = Albums for this +in have been updated.
		zh = 此图的标签已更新。
    [timeline_source]
        en = Image Source
		zh = 图片来源
    [view_page]
        en = View Source
		zh = 浏览来源
    [points_earned_today]
        en = Earned %d points today
		zh = 今天获得了%d积分
    [report_content]
        en = Report Abuse
		zh = 举报
	[comment_count]
		en = %d comments
	[view_description]
		en = View description
	[write_comment_button]
		en = Write Comment
	[write_comment_field_placeholder]
		en = Write a Comment...
	[post_comment_button]
	    en = Post

[[Report Content Screen]]
    [report_content_reason]
        en = Reason for Report

[[Choose Tags Screen]]
	[choose_tags_to_add]
		en = Choose Albums to Add
		zh = 选择需要添加的标签
    [tag_search_placeholder]
        en = #album
		zh = #标签
	[edit_tags]
		en = Edit
		zh = 编辑标签
	[tags_chosen]
		en = Albums Chosen
		zh = 已选择的标签
	[post_in]
		en = Post
		zh = OK
    [post_in_button]
        en = Post +in
	[save_in]
		en = Save
		zh = 保存
	[tag_already_added]
		en = You have already added this album.
		zh = 您已经添加过了这个标签。
	[no_tags_added]
		en = Please add at least one album before posting.
		zh = 在您收藏前请先添加标签。
    [clear_all_tags]
        en = Clear All
		zh = 清除全部
    [clear_all_tags_confirmation]
        en = Clear all albums?
		zh = 是否清除全部标签？
    [uploading]
        en = Uploading...
    [ban_in_message]
        en = Your account is banned by one or more of following reasons.\nYou are temporarily blocked from using some features.\n\n・Spam\n・Nudity or Pornography\n・Graphic Violence\n・Low Pixel Density\n・Low Quality Wallpaper or Scan\n・Wrong or Random Album(s)\n\n*You will be unblocked in few days.
        zh = 你的账户因为一个或数个理由被封禁了。\n您已被限制使用部分功能一段时间。\n\n・垃圾图片\n・裸露或者色情图片\n・露骨的暴力图片\n・低像素图片\n・低质量图片\n・错误或者随机的图集\n\n*您将被封禁数日。
    [reward_points]
        en = Reward points
        zh = 奖品积分

[[Main Menu]]
	[menu_title_home]
		en = Home
		zh = 主页
		in = Rumah
		id = Rumah
	[menu_title_profile_ins]
		en = Me
		zh = 资料和图集
		in = Profil & +ins
		id = Profil & +ins
	[menu_title_upload]
		en = Upload
		zh = 上传
		in = Unggah
		id = Unggah
	[menu_title_rewards]
		en = Store
		zh = 奖品
		in = Hadiah
		id = Hadiah
    [menu_title_discussion]
        en = Discussion
    [menu_title_buy_points]
        en = Buy Points
	[menu_title_orders]
		en = Orders
		zh = 订单
		in = Pesanan
		id = Pesanan
	[menu_title_settings]
		en = Settings
		zh = 设置
		in = Pengaturan
		id = Pengaturan
	[menu_title_logout]
		en = Log Out
		zh = 登出
		in = Keluar
		id = Keluar
	[menu_title_help]
		en = Help
		zh = 帮助
		in = Bantuan
		id = Bantuan
	[menu_title_bonus]
		en = Bonus
		zh = 奖励
    [menu_title_search]
        en = Search
        zh = 搜索
        in = Cari
        id = Cari
	[menu_title_fan_review]
		en = Fan Review
    [search_users]
		en = Search
		zh = 搜索
		in = Cari
		id = Cari
    [menu_title_invite]
        en = Invite Friends
    [menu_title_events]
        en = Events
    [menu_title_popular_tags]
        en = Browse Tags
    [menu_title_auctions]
    	en = Auctions
    [menu_title_lottery]
    	en = Lottery

[[Settings]]
	[logout_confirm]
		en = Are you sure you want to log out?
		zh = 是否确定登出？
	[terms_of_service]
		en = Terms of Service
		zh = 使用规则
    [privacy_policy]
        en = Privacy Policy
		zh = 隐私条款
    [about_ads]
        en = About In-App Ads
	[unregister_button]
		en = Unregister Account
		zh = 注销账户
	[unregister_confirm_title]
		en = Confirm Unregister Account
		zh = 确认注销账户
	[unregister_confirm_message]
		en = Unregistering your account will cause you to lose all your posted +ins and reward points.
		zh = 注销用户意味着您将失去所有收藏的图片和所有的积分。
	[settings_heading_profile]
		en = Profile
		zh = 资料
	[settings_heading_account]
		en = Account
		zh = 账户
	[settings_heading_notifications]
		en = Notifications
		zh = 通知
	[settings_heading_links]
		en = Links
		zh = 链接
	[username]
		en = Username
		zh = 用户名
	[nickname]
		en = Nickname
		zh = 昵称
	[language]
		en = Language
		zh = 语言
	[receive_notifications]
		en = Receive Notifications
		zh = 接收通知
	[save_changes_button]
		en = Save Changes
		zh = 保存修改

[[Rewards]]
	[left_in_stock]
		en = Only %d left in stock
		zh = 仅存%d在库
	[out_of_stock]
		en = Out of stock
		zh = 无货
	[reward_details]
		en = Reward Details
		zh = 奖品详细
	[price_label]
		en = %@ Points
		zh = %@积分
	[free_shipping]
		en = & Free Shipping
		zh = 免费邮寄
	[not_enough_points]
		en = You don't have enough points to purchase this item!
		zh = 您没有足够的积分兑换此奖品。
    [no_rewards]
        en = No rewards to display.
        zh = 现在没有奖品
    [choose_filter]
        en = Choose a Filter
    [clear_filter]
        en = Clear
    [order_now]
        en = Order now
        zh = 订购
    [user_review]
        en = User Reviews
    [reviewer_count]
        en = %d people have reviewed this reward.
    [no_reviews_yet]
        en = No reviews yet.
    [add_to_wishlist]
    	en = Add To Wishlist
    [wishlist_text_prompt]
    	en = What do you like about this reward?
    [wishlist_point_prompt]
    	en = How much would you like to spend on this reward (in points)?

[[Orders]]
	[order_completed_message]
		en = Your order has been successfully completed. Thank you for ordering!\nPlease enjoy!
		zh = 您已经成功下单。感谢您的订购。\n祝您玩的愉快！
	[go_to_order_list]
		en = Go to your order list
		zh = 前往订单列表
	[edit_order_address]
		en = Edit Address
		zh = 编辑地址
	[save_changes]
		en = Save Changes
		zh = 保存修改
	[place_order]
		en = Place This Order
		zh = 订购
	[uneditable_address]
		en = The address of this order can no longer be edited.
		zh = 此订单的地址已经无法更改。
	[address_updated]
		en = The address of this order has been updated.
		zh = 此订单的地址已更新。
	[no_orders]
		en = You don’t have any orders.
		zh = 您还没有订单。
    [shipping_address]
        en = Shipping Address
		zh = 邮送地址
	[write_review_redeem]
		en = Write Review
	[review_description]
		en = Write a review about this reward so other people can benefit from your expertise.\nWrite a review in English more than 100 characters.
	[send_review]
		en = Send Review

[[Address Form]]
	[required_field_missing]
		en = %@ can’t be blank.
		zh = %@ 不能为空
	[field_not_alphabet]
		en = %@ has non-alphabet characters.
		zh = %@ 不是英文
	[enter_address]
		en = Enter Your Address
		zh = 输入您的地址
	[first_name]
		en = First Name
		zh = 名字
	[last_name]
		en = Last Name
		zh = 姓氏
	[street_address]
		en = Street Address
		zh = 地址
	[city]
		en = City
		zh = 城市
	[state]
		en = State / Province
		zh = 州/省
	[postcode]
		en = Zip / Postal Code
		zh = 邮编
	[country]
		en = Country
		zh = 国家

[[User Profile]]
	[profile_nav_title]
		en = %@’s Profile
		zh = %@的个人资料
	[confirm_delete_tag]
		en = Are you sure you want to delete the album '%@'?
		zh = 是否确定删除标签 '%@'？
    [confirm_delete_tag_android]
        en = Are you sure you want to delete this album?
		zh = 是否确定删除此标签？
	[delete_tag_button]
		en = Delete Album
		zh = 删除标签
    [today]
        en = today
        comment = Suffix shown after number of points earned today. Example: ▲ 100 today
		zh = 今天
    [pick_image]
        en = Pick an Image
		zh = 选取一张图片
    [no_ins]
        en = You don't have any +ins yet.
		zh = 您还没有+in任何图片。

[[Sharing]]
	[posted_facebook]
		en = Successfully posted to Facebook
		zh = 已成功分享到Facebook
	[unable_to_post_facebook_title]
		en = Unable to post to Facebook
		zh = 分享到Facebook失败
	[unable_to_post_facebook_msg]
		en = Please set up Facebook on your device first.
		zh = 请先设置您设备里的Facebook
    [invite_facebook]
        en = Invite(s) sent.

[[User Search]]
    [user_search_placeholder]
        en = username
		zh = 用户名
    [no_such_user]
        en = No such user.
		zh = 没有更多的用户了。

[[Rate App Dialog]]
    [rate_dialog_title]
        en = Rate Anime i.ntere.st
		zh = 给动漫图藏评分
    [rate_dialog_message]
        en = If you like using Anime i.ntere.st, please take a moment to rate it. Thank you for your support!
		zh = 如果您喜欢动漫图藏或有建议，请花几分钟给我们打分吧。感谢您的支持！
    [rate_dialog_ok]
        en = Rate now
		zh = 现在就去！
    [rate_dialog_cancel]
        en = Maybe later
		zh = 等会再说吧
    [rate_dialog_no]
        en = No thanks
		zh = 不评就是不评

[[Tutorial]]
    [dialog_menu]
        en = Open menu.
		zh = 打开菜单。
    [dialog_profile]
        en = User who +in-ed image shown
		zh = +in了此图片的用户
    [dialog_in]
        en = +in lets you add images\nto your collection
		zh = +in能让您添加图片\n到您的收藏里
    [dialog_swipe]
        en = Swipe left to browse more images
		zh = 向左滑动浏览更多图片

[[Fan Review]]
    [review_count]
        en = %d people have reviewed.
    [write_review]
        en = Write a Review
    [write_review_field_title]
    	en = Write a review (at least 100 characters)
    [no_questions]
    	en = No fan reviews to display.
    [choose_rating]
    	en = Choose a Rating
    [choose_age]
    	en = How old are you?
    [choose_gender]
    	en = Are you male or female?
    [choose_country]
    	en = Where do you live?

[[Ratings]]
    [rating_1]
        en = Pathetic
    [rating_2]
        en = Dreadful
    [rating_3]
        en = Poor
    [rating_4]
        en = Decent
    [rating_5]
        en = Mediocre
    [rating_6]
        en = Fair
    [rating_7]
        en = Good
    [rating_8]
        en = Very Good
    [rating_9]
        en = Great
    [rating_10]
        en = Outstanding

[[Event]]
    [event]
        en = Event

[[Android Only]]
    [app_name]
        en = i.ntere.st
        zh = 动漫图藏
    [navigation_drawer_open]
        en = Open navigation drawer
        zh = 打开导览选单
    [navigation_drawer_close]
        en = Close navigation drawer
        zh = 关闭导览选单
    [point_format]
        en = %s%s points%s
        zh = %s%s 积分%s
    [continue_with_facebook]
        en = Continue with Facebook
        zh = 连接facebook
    [facebook_app_id_release]
        en = 173434652707123
        zh = 173434652707123
    [search]
        en = Search
        zh = 检索
    [share_with]
        en = Share With...
        zh = 分享到...
    [download_img_confirmation]
        en = Do you want to download this image?
        zh = 是否下载此图片？
    [downloaded_message_format]
        en = %1$s is saved to %2$s folder
        zh = %1$s 已被保存到 %2$s 文件夹
    [ask_overwrite_downloaded_image_file_format]
        en = %s already exists. Do you want to overwrite the current file?
        zh = %s 已经存在。是否覆盖当前文件？
    [failed_to_save_image_format]
        en = Failed to save %1$s in %2$s folder
        zh = 无法保存 %1$s 到 %2$s 文件夹
    [failed_to_download_image]
        en = Failed to download image
        zh = 下载图片失败
    [failed_to_access_facebook]
        en = Failed to access facebook
        zh = 连接facebook失败
    [post]
        en = POST
        zh = OK
    [in_success_message]
        en = +in completed
        zh = +in 完成
    [in_success_message_no_point_detail]
        en = +in completed
        zh = +in 完成
    [in_success_message_upload]
        en = Your image will be uploaded.Check progress in the notifications window.
        zh = +in 完成
    [download_started]
        en = Download started...
        zh = 开始下载...
    [download_failed]
        en = Download failed...
        zh = 下载失败...
    [choose_app_to_pick_image]
        en = Choose app to pick image.
        zh = 选择上传图片的app。
    [in]
        en = +in...
        zh = +in...
    [in_rooted]
        en = (Suspect bot.)\nYou must use an unrooted\nAndroid device to gain\n points by +in-ing
        zh = （bot嫌疑）\n您必须使用未经ROOT的\n安卓设备通过+in来获取\n 积分。
    [full_address_format_first_name_last_name_street_city_state_zip_country_in_this_order]
        en = %1$s %2$s\n%3$s\n%4$s, %5$s %6$s\n%7$s
        zh = %1$s %2$s\n%3$s\n%4$s, %5$s %6$s\n%7$s
    [order_detail]
        en = Order details
        zh = 订单详情
    [reward_notification_title_format]
        en = %1$s!
        zh = %1$s!
    [reward_notification_message_format]
        en = Now you can redeem %1$s!
        zh = 现在您可以兑换 %1$s!
    [upload_completed_message_format]
        en = Upload Completed\n%s%d%s
        zh = 下载完成\n%s%d%s
    [faq]
        en = FAQ
		zh = FAQ
    [notification]
        en = Notification
		zh = 通知
    [file_not_found]
        en = File not found...
		zh = 找不到文件...
    [download_completed]
        en = Download completed...
		zh = 下载完成...
    [select_country]
        en = Select country
		zh = 选择国家
    [unregister_completed]
        en = Unregister completed
	    zh = 注销完成
    [contact_us]
        en = Contact Us
		zh = 联系我们
    [reward_keyword_placeholder]
        en = Keyword
        zh = 关键词
    [search_tag_placeholder]
        en = Album Name
        zh = 标签
    [no_such_tag]
        en = No such album.
    [in_for_in]
        en = Invite
    [choose_share]
        en = Choose app to share image.
    [open_with]
        en = Open with ...
    [report_in]
        en = Report
    [copy_link]
        en = Copy Link
    [share]
        en = Share
    [download]
        en = Download
    [edit]
        en = Edit
    [delete]
        en = Delete
    [at_place]
        en = at %s
    [popular_tag]
        en = Browse
    [l_content_desc_open_drawer]
        en = Open menu
    [l_content_desc_close_drawer]
        en = Close menu
    [l_action_finish_select]
        en = Done
    [l_action_cancel_select]
        en = Cancel
    [l_confirm_dialog_title]
        en = Attention
    [l_confirm_dialog_message]
        en = Are your sure you want to discard the selection?
    [l_format_selection_count]
        en = Selected items(%1$d / %2$d)
    [l_error_invalid_format]
        en = Unable to select this image due to invalid file format.
    [l_error_quality]
        en = Unable to select this image due to low quality. Please select an image with higher quality.
    [l_album_name_all]
        en = All Photos
    [l_album_name_selected]
        en = Selected Photos
    [l_album_name_camera]
        en = Camera
    [l_album_name_download]
        en = Download
    [l_album_name_screen_shot]
        en = Screenshot
    [l_empty_photo]
        en = No photo.
    [l_empty_selection]
        en = Nothing selected.
    [l_action_counter]
        en = 0

[[Android HTML Errors]]
    [not_found_message]
        en = 404 Not Found
		zh = 404 找不到网页
    [internal_server_error_message]
        en = 500 Internal Server Error
		zh = 500 网络服务出错
    [unprocessable_entity_message]
        en = 422 Unprocessable Entity
		zh = 422 无法响应
    [conflict_message]
        en = 409 Conflict
		zh = 409 请求冲突
    [bad_request_message]
        en = 400 Bad Request
		zh = 400 错误的请求
    [forbidden_message]
        en = 403 Forbidden
		zh = 403 禁止访问
